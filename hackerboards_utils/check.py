#!/usr/bin/env python3
import argparse
import os.path
from pathlib import Path

import yaml
from tabulate import tabulate

import hackerboards_utils.schema as schema


def generate_typedict():
    typedict = {}
    for field in schema.fields:
        typedict[field['name']] = field['type']
    return typedict


def generate_relations():
    relations = {}
    root = Path(os.path.dirname(__file__)).parent / "dataset"
    for field in schema.fields:
        if 'relation' in field:
            relations[field['name']] = {
                'relation': field['relation'],
                'options': []
            }
            if os.path.isfile(root / field['relation']):
                with open(root / field['relation'], 'r') as handle:
                    raw = yaml.load(handle, Loader=yaml.CLoader)
                for row in raw:
                    relations[field['name']]['options'].append(row['name'])
    return relations


def check_value(boarddata, key, value, note=None, human=False):
    if not human:
        return
    try:
        if boarddata[key] is not None and boarddata[key] > value:
            print("Please check the value of ", key, " value ", boarddata[key], ">", value)
            if note is not None:
                print(" ->", note)
    except ValueError:
        # key not there, don't check
        pass


td = generate_typedict()
relations = generate_relations()


def check(boardfile, human=False):
    with open(boardfile, 'r') as handle:
        boarddata = yaml.load(handle, Loader=yaml.CLoader)

    # Check Types
    type_errors = []
    unset_errors = []
    relation_errors = []
    for key in td:
        schematype = td[key]

        # Get value
        try:
            boardvalue = boarddata[key]
            boardvaluetype = type(boardvalue).__name__
        except KeyError:
            unset_errors.append([key, schematype, "<Not There>"])
            continue
        if boardvalue is None:
            continue

        # Check Type
        if schematype == "integer":
            if type(boardvalue) not in [int, float]:
                type_errors.append([key, schematype, boardvalue, boardvaluetype])
                # raw[key] = int(rawkey)
        elif schematype == "string":
            if type(boardvalue) != str:
                type_errors.append([key, schematype, boardvalue, boardvaluetype])
        elif schematype == "boolean":
            if type(boardvalue) != bool:
                type_errors.append([key, schematype, boardvalue, boardvaluetype])
                # if rawkey == 1:
                #     raw[key] = True
                # elif rawkey == 0:
                #     raw[key] = False
        elif schematype == "float":
            if type(boardvalue) not in [float, int]:
                type_errors.append([key, schematype, boardvalue, boardvaluetype])

        # Check relation
        if key in relations:
            values = [boardvalue]
            if schematype == 'array':
                values = boardvalue
            for v in values:
                if v is not None and v not in relations[key]['options']:
                    relation_errors.append(f'Value "{v}" is not defined in {relations[key]["relation"]}')

    # Check Power
    check_value(boarddata, "power_input_current_max", 10, "current (float) is in A not mA", human)
    check_value(boarddata, "power_input_current_min", 10, "current (float) is in A not mA", human)
    check_value(boarddata, "power_input_voltage_max", 230, "voltage (float) is in V not mV", human)
    check_value(boarddata, "power_input_voltage_min", 230, "voltage (float) is in V not mV", human)

    # Check Price
    check_value(boarddata, "_price", 500, "price (integer) is in dollar not cent", human)

    # Check Memory
    check_value(boarddata, "memory_max", 1024 * 1024, "memory (integer) is in MBi not in Byte", human)  # 1TBi

    if len(type_errors) > 0 or len(unset_errors) > 0 or len(relation_errors) > 0:
        print()
        print(f"Error on board [{boardfile}]:")
    # Type Errors
    if human or len(type_errors) > 0:
        print(tabulate(type_errors, headers=["schema key", "schema type",
                                             " yaml value", " yaml type"], disable_numparse=True))
    if human or len(relation_errors) > 0:
        for line in relation_errors:
            print(f"- {line}")
    if human or len(unset_errors) > 0:
        for missing in unset_errors:
            print(f"- missing {missing[0]} [{missing[1]}]")
    return {'type': type_errors, 'missing': unset_errors, 'relations': relation_errors}


def main():
    parser = argparse.ArgumentParser(description="Check Hackersbords-YAML for correctness")
    parser.add_argument("filename", type=str, help="YAML file to check")
    parser.add_argument('--keysnotset', action='store_true', help="show keys that are not set oder None")
    parser.add_argument('--typeunkown', action='store_true', help="show keys with unkown type to this tool")
    args = parser.parse_args()

    check(args.filename, human=True)


if __name__ == '__main__':
    main()
